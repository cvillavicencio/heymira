Isaac Asimov WORLD OF IDEAS
https://www.youtube.com/watch?v=6zdmbZcRd8w

zomg zufall
https://z0r.de/

Hitchcock explica el efecto Kuleshov
https://www.youtube.com/watch?v=Q1LmKtWAfOg

Make your own distro
https://blog.z3bra.org/2016/01/make-your-own-distro.html

https://ensondeluz.com/tag/erasmo-de-rotterdam/
https://ensondeluz.com/tag/erasmo-de-rotterdam/

Experiments with ffmpeg filters and frei0r plugin effects
https://yalantis.com/blog/experiments-with-ffmpeg-filters-and-frei0r-plugin-effects/

Manual de Vassal, motor de juegos de mesa
http://www.vassalengine.org/mediawiki/images/b/b0/Vassal_3.1_designerguide.pdf

10-20-30 rule of presenting
https://strategyumwelt.com/frameworks/102030-rule-of-presenting

música clásica
http://amp.cesnet.cz:8000/cro-d-dur-256.ogg

wonder showzen
https://kimcartoon.to/Cartoon/Wonder-Showzen

fortuna de arco
http://hex.propoleo.xyz/fortuna

Nameless - Una crítica de la identidad (lectura en inglés)
https://theanarchistlibrary.org/library/wolfi-landstreicher-nameless-an-egoist-critique-of-identity

Union of egoists
https://www.unionofegoists.com/

Random Geographic Coordinates
https://www.random.org/geographic-coordinates/

superbad
https://superbad.com

nombre colores html
https://www.w3schools.com/colors/colors_names.asp

css animaciones
https://css-tricks.com/css-animation-libraries/

SANSAR - Behçet Nacar &amp; Kazım Kartal (1975)
https://www.dailymotion.com/video/x7ysnfy

How did Neanderthals and other ancient humans learn to count?
https://www.nature.com/articles/d41586-021-01429-6

Lo que he aprendido: diagramas en Org mode con Ditaa
https://ondahostil.wordpress.com/2017/12/06/lo-que-he-aprendido-diagramas-en-org-mode-con-ditaa/

Casos de uso: include y extend
https://www.abiztar.com.mx/articulos/casos-a-incluir-casos-a-extender.html

A Hole in the Head: A History of Trepanation
https://thereader.mitpress.mit.edu/hole-in-the-head-trepanation/

Micro-Habits of Effective Managers
https://review.firstround.com/the-25-micro-habits-of-high-impact-managers

The elemental strangeness of foxes
https://www.plough.com/en/topics/justice/environment/the-elemental-strangeness-of-foxes

The elemental strangeness of foxes
https://www.plough.com/en/topics/justice/environment/the-elemental-strangeness-of-foxes

The Siberian unicorn lived at the same time as modern humans
https://www.nhm.ac.uk/discover/news/2018/november/the-siberian-unicorn-lived-at-the-same-time-as-modern-humans.html

Ribosome – A simple generic code generation tool
http://sustrik.github.io/ribosome/

I Know the Secret to the Quiet Mind. I Wish I’d Never Learned It
https://www.theatlantic.com/health/archive/2021/06/car-accident-brain-injury/619227/

Can programming be liberated from the von Neumann style? (1978)
https://dl.acm.org/doi/10.1145/359576.359579

Kazakhstan’s forest is the birthplace of modern apples
http://www.bbc.com/travel/story/20181120-the-birthplace-of-the-modern-apple

Bee-friendly urban wildflower meadows prove a hit with German city dwellers
https://www.theguardian.com/environment/2021/jun/20/bee-friendly-urban-wildflower-meadows-prove-a-hit-with-german-city-dwellers

Against Meme Activism
https://www.persuasion.community/p/against-meme-activism

erhard arendt
http://www.arendt-art.de/

James Jean
http://www.jamesjean.com/

Animals on the underground
https://animalsontheunderground.com/

Memory of the world
https://www.memoryoftheworld.org/

The hospital
http://hospital.apoka.com/

Blog de Papeles de los 70 para todo lo relacionado con el papel pintado
https://www.papelesdelos70.com/inspiracion/blog/

Cremaster
http://www.cremaster.net

Autos de policía
http://www.polizeiautos.de/

Grifos
http://www.firehydrant.org/pictures/index.html

Python para todos: Diferencia entre método y función
https://empresas.blogthinkbig.com/python-para-todos-metodo-vs-funcion/

Documental sobre el bosque esclerófilo de Chile
https://www.youtube.com/watch?v=D0flkeJAqv8

superbad
https://superbad.com

Animals on the underground
https://animalsontheunderground.com/

How to think about pleasure
https://psyche.co/guides/from-hedonism-to-humanism-philosophys-defence-of-pleasure

Sembrar agua
https://www.youtube.com/watch?v=O_xSr9RQdUM


