<?php
include('top.php');
include('lin.php');

$formnl = '<form action="nli.php" method="POST">
    Título:<br><input name="nltxt" required><br><br>
    Url:<br><input type="url" placeholder="http://, https://, etc " name="nlurl"><br><br>
    Descripción:<br><textarea id="n_com" name="nlpre" required></textarea><br><br>
';

// inicio grupos
$formnl .= 'Grupo:<br><select name="nlgru">

   <optgroup label="Grupos públicos">';

$formnl .= '<option value="1">'.debd($bd_connect, 'gttl', 'grupos', 'id', 1).'</option>
</optgroup>';
// acá se permite escoger un grupo en el que va a compartir
$cuantosgrup = lsbd($bd_connect,'id','grupos');

$grupospers = '';
for ($i = 2; $i <= $cuantosgrup; $i++ ){
    // verifica que usuario esté en grupo
    // pasa por los grupos

    $nl_aut = debd($bd_connect, 'id', 'usuarios', 'uhsh', $i_hashco);
    $engrupo = debd($bd_connect, 'egus','engrupo', 'egus', $nl_aut);

    if ($engrupo) { //existe el registro del usuario en el grupo
        // muestra grupo
        $gruponom = debd($bd_connect, 'gttl', 'grupos', 'id', $i);
        $grupospers .= '<option value="'.$i.'">'.$gruponom.'</option>';
    }
}
if ($grupospers != ''){
    $formnl .= '<optgroup label="Grupos a los que perteces">'.$grupospers.'</optgroup>';
}
$formnl .= '</select>';
// fin grupos


// inicia categoría
$formnl.='<br><br>Categorías<br><div class="listaCategorias">';
$cuantascat = lsbd($bd_connect,'id','categorias');

for ($i = 1; $i <= $cuantascat; $i++ ){
    $catnom = debd($bd_connect, 'categ', 'categorias', 'id', $i);
    $catdes = debd($bd_connect, 'catde', 'categorias', 'id', $i);
    $formnl .= '<input name ="nlcat[]" type="checkbox" value="'.$i.'"><c onmouseout="this.innerHTML=\''.$catnom.'\';" onmouseover="this.innerHTML=\''.$catdes.'\';">'.$catnom.'</c><br>';
}
$formnl .= '</div> ';
// fin categorias


$formnl .= '<br>
<input type="submit" value="Compartir link">
</form>
';

include('bar.php');
echo '<div id="content">';
echo '<div class="nllink"><img class="imag" src="img/res/nlifr.png"><h1>Nuevo link</h1>';

if ($conectado = '1'){
    $nl_url = val('nlurl');
    if ($nl_url != '000'){
        $nl_gtx = obtenerTitulo($nl_url);
    }
    $nl_txt = val('nltxt');
    $nl_cat = val('nlcat');
    $nl_gru = val('nlgru');    
    $nl_pre = val('nlpre');
    if ($nl_pre == '000'){
        $nl_pre = '<small>ººº</small>';
    }
    $nl_dat = date('Y-m-d H:m:s');
    $nl_aut = debd($bd_connect, 'id', 'usuarios', 'uhsh', $i_hashco);
    if($nl_txt != '000'){
        if ($nl_cat == '000'){
            $nl_cat = "1";
        }
        if ($nl_gru == '000'){
            $nl_gru = "1";
        }


        if (!debd($bd_connect, 'id', 'links', 'lurl', $nl_url)){
            $cbd = 'nli';
            include('cbd.php');
            echo 'Tu link fue agregado. Gracias!<br><br><a href="./?link='.$nl_id.'">Ir al link</a><br><a href=".">Volver al inicio</a>.<br><br>';
        } else {
            echo 'El link que estás enviando ya fue compartido por alguien más.<br><br><a href="./?link='.debd($bd_connect, 'id', 'links', 'lurl', $nl_url).'">Ir al link</a> (enviado por otro usuario) <br><a href="nli.php">Enviar otro link</a><br><a href=".">Volver al inicio</a><br><br>';
        }
        
    } else {
        echo $formnl;
    }
} else {
    echo $noveas;
}
echo '</div></div>';

include('bot.php')
?>
