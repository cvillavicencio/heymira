<?php
include('top.php');
include('bar.php');


$ed_link = val('lnk');
$ed_txt=debd($bd_connect, 'ltxt', 'links', 'id', $ed_link);
$ed_url=debd($bd_connect, 'lurl', 'links', 'id', $ed_link);

$formnl = '<div class="content"><div class="nllink"><img class="imag" src="img/res/nlifr.png"><h1>Editar link</h1><form method="post" action=".">
    Nueva Descripción:<br><input name="nltxt" value="'.$ed_txt.'" required><br><br>
    Nueva Url:<br><input type="url" placeholder="http://, https://, etc " name="nlurl" value="'.$ed_url.'"><br><br>
';

// inicio grupos
$formnl .= 'Grupo:<br><select name="nlgru">

   <optgroup label="Grupos públicos">';

$formnl .= '<option value="1">'.debd($bd_connect, 'gttl', 'grupos', 'id', 1).'</option>
</optgroup>';
// acá se permite escoger un grupo en el que va a compartir
$cuantosgrup = lsbd($bd_connect,'id','grupos');

$grupospers = '';
for ($i = 2; $i <= $cuantosgrup; $i++ ){
    // verifica que usuario esté en grupo
    // pasa por los grupos

    $nl_aut = debd($bd_connect, 'id', 'usuarios', 'uhsh', $i_hashco);
    $engrupo = debd($bd_connect, 'egus','engrupo', 'egus', $nl_aut);

    if ($engrupo) { //existe el registro del usuario en el grupo
        // muestra grupo
        $gruponom = debd($bd_connect, 'gttl', 'grupos', 'id', $i);
        $grupospers .= '<option value="'.$i.'">'.$gruponom.'</option>';
    }
}
if ($grupospers != ''){
    $formnl .= '<optgroup label="Grupos a los que perteces">'.$grupospers.'</optgroup>';
}
$formnl .= '</select>';
// fin grupos


// inicia categoría
$formnl.='<br><br>Categorías<br><div class="listaCategorias">';
$cuantascat = lsbd($bd_connect,'id','categorias');

for ($i = 1; $i <= $cuantascat; $i++ ){
    $catnom = debd($bd_connect, 'categ', 'categorias', 'id', $i);
    $formnl .= '<input name ="nlcat[]" type="checkbox" value="'.$i.'">'.$catnom.'<br>';
}
$formnl .= '</div> ';
// fin categorias


$formnl .= '<br>
<input type="submit" value="Compartir link">
</form>
</div>
</div> ';


echo $formnl;

?>
