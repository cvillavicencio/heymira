<?php
include ('cfg.php');

/*
u usuarios
l links
p puntos
c categorias*
g grupos
eg usuario en en grupo
ig ingreso grupo


*/

echo '<h1>instalación de heymira</h1>';

// usuarios
$sql = "CREATE TABLE usuarios (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
uusr VARCHAR(30) NOT NULL,
ucor VARCHAR(30) NOT NULL,
univ TINYINT NOT NULL,
upts DECIMAL NOT NULL,
uptd DECIMAL NOT NULL,
ulnk INT NOT NULL,
ucom INT NOT NULL,
ucla VARCHAR(500) NOT NULL,
uhsh VARCHAR(500) NOT NULL,
uimg VARCHAR(500) NOT NULL,
registro TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
ultimacc TIMESTAMP NOT NULL
)
";

if ($bd_connect->query($sql) === TRUE) {
  echo "Tabla usuarios ok";
} else {
  echo "Error creating table: " . $bd_connect->error;
}

echo '<hr>';



// links
// lpre-sentacion (parrafo q escribe usuario para presentar su link)
// AGREGAR EN FORMULARIO. URGENTE.
// nota sobre lvis: 1: pineado - 2: normal -  3: nolistado
// lgtx = es el titulo obtenido scraping

$sql2 = "CREATE TABLE links (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
lurl VARCHAR(200) NOT NULL,
ltxt VARCHAR(240) NOT NULL,
lpre VARCHAR(200),
laut VARCHAR(140) NOT NULL,
lcat VARCHAR(140) NOT NULL,
lgrp VARCHAR(140) NOT NULL,
lpts INT NOT NULL,
lvis INT NOT NULL, 
lcom INT NOT NULL,
lgtx VARCHAR(300),
ldat TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
lupt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL
)
";

if ($bd_connect->query($sql2) === TRUE) {
    echo "Tabla de links ok";
} else {
    echo "Error creating tabla de links: " . $bd_connect->error;
}

echo '<hr>';

// categorías
$sql3 = "CREATE TABLE categorias (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
categ VARCHAR(50) NOT NULL,
catde VARCHAR(300) NOT NULL,
catpt INT NOT NULL
)";

if ($bd_connect->query($sql3) === TRUE) {
    echo "Tabla de categorías ok";
} else {
    echo "Error creating tabla de categorias: " . $bd_connect->error;
}
echo '<hr>';

// grupos 
$sql3 = "CREATE TABLE grupos (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
gttl VARCHAR(50) NOT NULL,
gdes VARCHAR(300),
guss INT,
gvis INT NOT NULL,
gniv INT NOT NULL
)";

if ($bd_connect->query($sql3) === TRUE) {
    echo "Tabla de grupos ok";
} else {
    echo "Error creating tabla de categorias: " . $bd_connect->error;
}
echo '<hr>';

// usuarios entran a grupo
/* 14/07/21
egus: id usuario
eggr: grupo al que entra
egnu: nivel de usuario (defecto: 1). esto servirá para gestión de grupos
egda: fecha
*/

$sql3 = "CREATE TABLE engrupo (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
egus INT NOT NULL,
eggr VARCHAR(300),
egnu INT, 
egda TIMESTAMP
)";

if ($bd_connect->query($sql3) === TRUE) {
    echo "Tabla de engrupo ok";
} else {
    echo "Error creating tabla de engrupo: " . $bd_connect->error;
}
echo '<hr>';


// puntos

$sql2 = "CREATE TABLE puntos (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
pusr VARCHAR(200) NOT NULL,
plnk VARCHAR(240) NOT NULL,
pcom VARCHAR(240),
pgrp VARCHAR(240)
)
";

if ($bd_connect->query($sql2) === TRUE) {
    echo "Tabla de puntos ok";
} else {
    echo "Error creating tabla de puntos: " . $bd_connect->error;
}

echo '<hr>';


// ingresogrp

$sql2 = "CREATE TABLE ingresogrp (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
iusr VARCHAR(200) NOT NULL,
igrp VARCHAR(240) NOT NULL,
idat TIMESTAMP
)
";

if ($bd_connect->query($sql2) === TRUE) {
    echo "Tabla de puntos ok";
} else {
    echo "Error creating tabla de puntos: " . $bd_connect->error;
}

echo '<hr>';


// comentarios
$sql2 = "CREATE TABLE comentarios (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
clnk VARCHAR(50) NOT NULL,
caut VARCHAR(50) NOT NULL,
ctxt VARCHAR(10000) NOT NULL,
cpts DECIMAL NOT NULL,
cvis TINYINT,
ccom INT,
cdcm TINYINT,
cdat TIMESTAMP
)";

if ($bd_connect->query($sql2) === TRUE) {
    echo "Tabla de comentarios ok";
} else {
    echo "Error creating tabla de comentarios: " . $bd_connect->error;
}



?>

