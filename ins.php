<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('cfg.php');
include('fxs.php');

$formis='<div id="content"><div class="nllink">
<img class="imag" src="./img/res/insfr.png">
<h1>Inicia sesión</h1><form action="ins.php?b=no" method="POST">
    Nombre de usuario:<br><input name="i_user" required><br><br>
    Contraseña:<br><input name="i_pass" type="password" required><br><br><small><label><input type="checkbox" name="i_mtct" value="1"> Mantener mi cuenta abierta en otros dispositivos. (inseguro)</small></label><br><br>
    <input type="submit" value="Ingresar">
    </form>
    <br><small>¿No tienes una cuenta? <a href="reg.php">Crea una!</a></small><br><br></div></div>
    ';


$i_user = val('i_user');
$i_pass = val('i_pass');
if ($i_user != '000' && $i_pass != '000'){
    $i_pass = hash('sha512',$i_pass);
    $b_pass = debd($bd_connect, 'ucla', 'usuarios', 'uusr', $i_user);
    if ($i_pass == $b_pass){ // coincide hash de clave guardada con hash de clave ingresada
        if (val('i_mtct') == 1){
            $c_hash = debd($bd_connect,'uhsh','usuarios','uusr', $i_user);
        } else {
            $c_hash = rand(10000,99999).rand(10000,99999);
        }
        $c_xpir = time() + 60 * 60 * 24 * 30; // expira en un mes
        setcookie("user", $i_user, $c_xpir);
        setcookie("hash", $c_hash, $c_xpir);
        $cbd = 'ins';
        include('cbd.php');
        
        //        include('lin.php');
        $conectado = 1;
        include('bar.php');

        echo '<div id="content"><div class="nllink"><img class="imag" src="./img/res/insok.png"><h1>Hola!</h1>Has iniciado sesión!<br><br><a href=".">Ir al inicio.</a><br><br></div></div>';
    } else {
        include('lin.php');
        include('bar.php');
        echo '<div id="content"><div class="nllink"><img class="imag" src="./img/res/no.png"><h1>Error!</h1><br>contraseña equivocada<br><br>
<a href=".">Ir al inicio</a> - <a href="ins.php">Intentar de nuevo.</a><br><br></div></div>';
    }
} else {
    include('lin.php');
    include('bar.php');
    echo $formis;
}

?>
